const express = require('express');
const router = express.Router();
//data = require('../data/flashcardData.json').data; ES6
const { data } = require('../data/flashcardData.json');
//cards = data.cards
const { cards } = data;

//the name of the URL will be /cards because of the name of the js file and not card as the pug file
router.get('/:id', (req, res) => {
    //side = req.query.side
    const { side } = req.query;
    //id = req.params.id
    const { id } = req.params; 
    const text = cards[id][side];
    // hint = cards[id].hint
    const { hint } = cards[id];
    var templateData = { text, side, id };
    if( side === 'question'){
        templateData.hint = hint;
    }
    
    //use template data to give all the info at the same time
    //res.locals.prompt = cards[req.params.id].question;
    //.res.locals.hint = cards[req.params.id].hint;
    res.render('card' , templateData);
});

module.exports = router;