const express = require('express');
//mini-app in express
const router = express.Router();

router.get('/', (req, res) => {
    const name = req.cookies.username;
    if (name) {
        res.render('index', { name: name });
    } else {
        res.redirect( '/hello', 302);
    }
});



router.get('/hello', (req, res) => {
    const name = req.cookies.username;
    if (name) {
        res.redirect('/', 302);
    } else {
        res.render('hello')
    }
});

router.post('/hello', (req, res) => {
    res.cookie('username', req.body.username);
    res.redirect('/');
});

router.post('/goodbye', (req, res) => {
    res.clearCookie('username');
    res.redirect('/hello', 302);
});

module.exports = router;
