const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
//creates and returns an express application
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

var name = '';

//sets the view engine to the parameter pug. the method defines different settings in express. by default, it looks in views
app.set('view engine', 'pug');

//because the file is called index, there is no need to add it to the path
const mainRoutes = require('./routes');
const cardRoutes = require('./routes/cards.js');

app.use(mainRoutes);
app.use('/cards', cardRoutes);

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});



app.use((err, req, res, next) => {
    res.locals.error = err;
    res.status(err.status);
    res.render('error');
});



//creates a server on port 3000
app.listen(3000, () => {
    console.log("The application is running on localhost:3000");
});
